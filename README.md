# Test CodeQuality CI Component

Serves as a demo project to verify code-quality [CI Component](https://gitlab.com/gitlab-components/code-quality) is working in line with the equivelant [CI template](https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/lib/gitlab/ci/templates/Jobs/Code-Quality.gitlab-ci.yml).
